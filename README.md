<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.90 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.12 -->
# parse_date 0.3.5

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_parse_date/develop?logo=python)](
    https://gitlab.com/ae-group/ae_parse_date)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_parse_date/release0.3.4?logo=python)](
    https://gitlab.com/ae-group/ae_parse_date/-/tree/release0.3.4)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_parse_date)](
    https://pypi.org/project/ae-parse-date/#history)

>ae namespace module portion parse_date: parse date strings more flexible and less strict.

[![Coverage](https://ae-group.gitlab.io/ae_parse_date/coverage.svg)](
    https://ae-group.gitlab.io/ae_parse_date/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_parse_date/mypy.svg)](
    https://ae-group.gitlab.io/ae_parse_date/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_parse_date/pylint.svg)](
    https://ae-group.gitlab.io/ae_parse_date/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_parse_date)](
    https://gitlab.com/ae-group/ae_parse_date/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_parse_date)](
    https://gitlab.com/ae-group/ae_parse_date/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_parse_date)](
    https://gitlab.com/ae-group/ae_parse_date/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_parse_date)](
    https://pypi.org/project/ae-parse-date/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_parse_date)](
    https://gitlab.com/ae-group/ae_parse_date/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_parse_date)](
    https://libraries.io/pypi/ae-parse-date)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_parse_date)](
    https://pypi.org/project/ae-parse-date/#files)


## installation


execute the following command to install the
ae.parse_date module
in the currently active virtual environment:
 
```shell script
pip install ae-parse-date
```

if you want to contribute to this portion then first fork
[the ae_parse_date repository at GitLab](
https://gitlab.com/ae-group/ae_parse_date "ae.parse_date code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_parse_date):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_parse_date/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.parse_date.html
"ae_parse_date documentation").
